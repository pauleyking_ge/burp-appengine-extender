package burp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;

public class BurpExtender implements IBurpExtender, IHttpListener {
    private IExtensionHelpers helpers;

    private PrintWriter stdout;
    private PrintWriter stderr;

    private Pattern cookiePattern;
    private Pattern authPattern;
    private Pattern csrfPattern;

    private String client;
    private String host;
    private String secret;

    @Override
    public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {
        helpers = callbacks.getHelpers();

        stdout = new PrintWriter(callbacks.getStdout(), true);
        stderr = new PrintWriter(callbacks.getStderr(), true);

        cookiePattern = Pattern.compile(Pattern.quote("cookie"), Pattern.CASE_INSENSITIVE);
        authPattern = Pattern.compile(Pattern.quote("authorization"), Pattern.CASE_INSENSITIVE);
        csrfPattern = Pattern.compile(Pattern.quote("CSRF-TOKEN-PS"), Pattern.CASE_INSENSITIVE);

        host = System.getProperty("UAA_HOST");
        client = System.getProperty("UAA_CLIENT");
        secret = System.getProperty("UAA_CLIENT_SECRET");

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1)
                    throws CertificateException {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            stderr.println(e.getLocalizedMessage());
        }

        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        javax.net.ssl.HttpsURLConnection
                .setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {

                    public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                        return true;
                    }

                });

        callbacks.setExtensionName("BurpUAAAuthenticator");
        callbacks.registerHttpListener(this);
    }

    @Override
    public void processHttpMessage(int toolFlag, boolean messageIsRequest,
            IHttpRequestResponse messageInfo) {

        if (messageIsRequest && toolFlag == IBurpExtenderCallbacks.TOOL_SCANNER) {
            String token = "";

            URL url;
            try {
                url = new URL(host + "/oauth/token");

                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                String urlParameters = "client_id=" + client + "&client_secret=" + secret
                        + "&grant_type=client_credentials" + "&response_type=token";

                byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

                OutputStream os = conn.getOutputStream();
                os.write(postData);
                os.flush();

                BufferedReader br = new BufferedReader(
                        new InputStreamReader((conn.getInputStream())));

                String output = "";

                String current;
                while ((current = br.readLine()) != null) {
                    output += current;
                }

                output += "\n";

                JSONObject obj = new JSONObject(output);
                token = obj.getString("access_token");

                conn.disconnect();
            } catch (IOException e) {
                stderr.println(e.getLocalizedMessage());
            }

            IRequestInfo iRequest = helpers.analyzeRequest(messageInfo);

            String request = new String(messageInfo.getRequest());

            boolean needsCSRF = true;

            String command = request.substring(0, request.indexOf(" "));
            switch (command) {
                case "GET":
                    needsCSRF = false;
                    break;
                default:
                    needsCSRF = true;
                    break;
            }

            List<String> headers = iRequest.getHeaders();
            List<String> newHeaders = new ArrayList<String>();

            String reqBody = request.substring(iRequest.getBodyOffset());

            boolean updated = false;
            for (int i = 0; i < headers.size(); i++) {
                String header = headers.get(i);

                if (cookiePattern.matcher(header).find()) {
                    continue;
                } else if (authPattern.matcher(header).find()) {
                    continue;
                } else if (csrfPattern.matcher(header).find()) {
                    continue;
                } else {
                    newHeaders.add(header);
                }
            }

            if (needsCSRF) {
                try {
                    String csrfServletPath = iRequest.getUrl().getProtocol() + "://"
                            + iRequest.getUrl().getHost() + "/director/CsrfServlet";

                    URL csrfURL = new URL(csrfServletPath);

                    HttpsURLConnection conn = (HttpsURLConnection) csrfURL.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("FETCH-CSRF-TOKEN", "1");
                    conn.setRequestProperty("Authorization", "Bearer " + token);

                    OutputStream os = conn.getOutputStream();
                    os.write(new byte[] {});
                    os.flush();

                    BufferedReader br = new BufferedReader(
                            new InputStreamReader((conn.getInputStream())));

                    String output = "";

                    String current;
                    while ((current = br.readLine()) != null) {
                        output += current;
                    }

                    newHeaders.add(output);
                } catch (IOException e) {
                    stderr.println(e.getLocalizedMessage());
                }
            }

            newHeaders.add("Authorization: Bearer " + token);

            byte[] message = helpers.buildHttpMessage(newHeaders, reqBody.getBytes());

            messageInfo.setRequest(message);
        }
    }
}
